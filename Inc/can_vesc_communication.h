/*
 * can_vesc_communication.h
 *
 *  Created on: 4 mar 2017
 *      Author: Kamil
 */

#ifndef CAN_VESC_COMMUNICATION_H_
#define CAN_VESC_COMMUNICATION_H_

#include "stm32f4xx_hal.h"


#define ALL_VESCs_ID 255

typedef enum {
	CAN_PACKET_SET_DUTY = 0,
	CAN_PACKET_SET_CURRENT,
	CAN_PACKET_SET_CURRENT_BRAKE,
	CAN_PACKET_SET_RPM,
	CAN_PACKET_SET_POS,
	CAN_PACKET_FILL_RX_BUFFER,
	CAN_PACKET_FILL_RX_BUFFER_LONG,
	CAN_PACKET_PROCESS_RX_BUFFER,
	CAN_PACKET_PROCESS_SHORT_BUFFER,
	CAN_PACKET_STATUS
} CAN_PACKET_ID;

struct VescStatus{
	int32_t Id;
	int32_t Rpm;
	int32_t Duty;
	int32_t Curent;
};


void CanVescInit();

//CAN recives all frame from all vescs
void FilterForAllVesc();

void ParseVescStatus(struct VescStatus* vescStatus);

void CanVescSendCommand(CAN_PACKET_ID Command, uint32_t Vescid, uint32_t Data);
void CanSend8bitCommand(CAN_PACKET_ID Command, uint32_t VescId, uint8_t Data);
void CanVescSendCommandToAllVescs(CAN_PACKET_ID Command, uint32_t Data);

#endif /* CAN_VESC_COMMUNICATION_H_ */
