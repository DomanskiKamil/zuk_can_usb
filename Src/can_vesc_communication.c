/*
 * can_vesc_communication.c
 *
 *  Created on: 4 mar 2017
 *      Author: Kamil
 */

#include "can_vesc_communication.h"

CanTxMsgTypeDef TxM;
CanRxMsgTypeDef RxM;
CAN_HandleTypeDef hcan;

CAN_FilterConfTypeDef sFilterConfig;

void CanVescInit(CAN_HandleTypeDef _hcan){
	hcan = _hcan;
	hcan.pTxMsg = &TxM;
	hcan.pRxMsg = &RxM;
}

void FilterForAllVesc(){
	sFilterConfig.FilterNumber = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0;
	sFilterConfig.FilterIdLow = CAN_PACKET_STATUS << 11 | CAN_ID_EXT;
	sFilterConfig.FilterMaskIdHigh = 0;
	sFilterConfig.FilterMaskIdLow = 0xF << 11; // Only Can_Packet_Status matter
	sFilterConfig.FilterFIFOAssignment = 0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 0;

	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
	//Interrupt enable
	HAL_CAN_Receive_IT(&hcan, CAN_FIFO0);
}

void FilterForTwosideMotors(){
	sFilterConfig.FilterNumber = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDLIST;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0;
	sFilterConfig.FilterIdLow = CAN_PACKET_STATUS << 11 | CAN_ID_EXT;
	sFilterConfig.FilterMaskIdHigh = 0;
	sFilterConfig.FilterMaskIdLow = CAN_PACKET_STATUS << 11 | 1<<3 |CAN_ID_EXT; // Only Can_Packet_Status matter
	sFilterConfig.FilterFIFOAssignment = 0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.BankNumber = 0;

	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
	//Interrupt enable
	HAL_CAN_Receive_IT(&hcan, CAN_FIFO0);
}
int16_t buffer_get_int16(const uint8_t *buffer, int32_t *index) {
	int16_t res =	((uint16_t) buffer[*index]) << 8 |
					((uint16_t) buffer[*index + 1]);
	*index += 2;
	return res;
}

int32_t buffer_get_int32(const uint8_t *buffer, int32_t *index) {
	int32_t res =	((uint32_t) buffer[*index]) << 24 |
					((uint32_t) buffer[*index + 1]) << 16 |
					((uint32_t) buffer[*index + 2]) << 8 |
					((uint32_t) buffer[*index + 3]);
	*index += 4;
	return res;
}


void buffer_append_uint32(uint8_t* buffer, uint32_t number, int32_t *index) {
	buffer[(*index)++] = number >> 24;
	buffer[(*index)++] = number >> 16;
	buffer[(*index)++] = number >> 8;
	buffer[(*index)++] = number;
}

void ParseVescStatus(struct VescStatus* vescStatus){

	vescStatus->Id = (hcan.pRxMsg->ExtId) & 0xFF;
	int32_t ind = 0;

	vescStatus->Rpm = buffer_get_int32(hcan.pRxMsg->Data,&ind);
	vescStatus->Curent = buffer_get_int16(hcan.pRxMsg->Data,&ind) ;
	vescStatus->Duty = buffer_get_int16(hcan.pRxMsg->Data,&ind) ;


}

/*
 * You can set DUTY, CURRENT, CURRENT_BREAK, RPM and POS
 * */
void CanVescSendCommand(CAN_PACKET_ID Command, uint32_t VescId, uint32_t Data){
	hcan.pTxMsg->ExtId = (Command << 8) | VescId;
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	hcan.pTxMsg->IDE = CAN_ID_EXT;
	hcan.pTxMsg->DLC = 4;

	int32_t index = 0;
	buffer_append_uint32(hcan.pTxMsg->Data, Data, &index);

	HAL_CAN_Transmit(&hcan,10);
}
void CanSend8bitCommand(CAN_PACKET_ID Command, uint32_t VescId, uint8_t Data){
	hcan.pTxMsg->ExtId = (Command << 8) | VescId;
	hcan.pTxMsg->RTR = CAN_RTR_DATA;
	hcan.pTxMsg->IDE = CAN_ID_EXT;
	hcan.pTxMsg->DLC = 1;

	int32_t index = 0;
	hcan.pTxMsg->Data[0] = Data;

	HAL_CAN_Transmit(&hcan,10);
}
void CanVescSendCommandToAllVescs(CAN_PACKET_ID Command, uint32_t Data){
	CanVescSendCommand(Command, ALL_VESCs_ID, Data);
}

